import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Admin, Resource } from "react-admin";
import simpleRestProvider from "ra-data-simple-rest";
import { PostList } from "./post";

class App extends Component {
  render() {
    return (
      <div className="">
        <Admin dataProvider={simpleRestProvider("http://localhost:8080")}>
          <Resource name="parents" list={PostList} />
          <Resource name="activites" list={PostList} />
          <Resource name="providers" list={PostList} />
        </Admin>
      </div>
    );
  }
}

export default App;
