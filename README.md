# thekidshub
kids hub is a platform connecting kids activities providers and parents.

##The Client App:
We bought a UI template and we made some changes on top of it. Still, we need to add Redux and connect to the API.

##The API:
We hired a backend developer and he made a good job for us. The developer excused for us to continue due to his reasons and he give us the code for free. 

##The Dashboard
We want to build it using the react-admin library.
https://marmelab.com/react-admin/

##We Are Looking:
We are looking for a full-stack developer to connect the dots and make the project live.
Also, we prefer a developer have the ability to continue with us as a full-time employee.


